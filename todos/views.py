from dataclasses import fields
from django.shortcuts import render
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView

from todos.models import TodoList, TodoItem
# Create your views here.


class TodoListView(ListView):
    model = TodoList
    template_name = 'todos/list.html'


class TodoDetailView(DetailView):
    model = TodoList
    template_name = 'todos/detail.html'


class TodoCreateView(CreateView):
    model = TodoList
    template_name = 'todos/new.html'
    fields = ["name", 'description', ]

    def get_success_url(self):
        return reverse_lazy("todos_detail", args=[self.object.id])


class TodoUpdateView(UpdateView):
    model = TodoList
    template_name = 'todos/edit.html'

    def get_success_url(self):
        return reverse_lazy("todos_detail", args=[self.object.id])


class TodoDeleteView(DeleteView):
    model = TodoList
    template_name = 'todos/delete.html'
    success_url = reverse_lazy("todos_list")
