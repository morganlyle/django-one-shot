from django.urls import path

from todos.views import (TodoListView, TodoDetailView,
                         TodoCreateView,
                         TodoUpdateView,
                         TodoDeleteView,
                         )

urlpatterns = [
    path('', TodoListView.as_view(), name="todos_list"),
    path("int:pk>detail/", TodoDetailView.as_view(), name="todos_detail"),
    path("todos/create/", TodoCreateView.as_view(), name="todos_new"),
    path("todos/<int:pk>edit/", TodoUpdateView.as_view, name="todos_update"),
    path("todos/<int:pk>/delete/", TodoDeleteView.as_view(), name="todos_delete"),
    # path('todos/items/new', )
]
